package com.sqlex.pegoopik.hello;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class LeadersCountRatingTest {
    private static final String SQL_END = "\n" +
            "\n" +
            "IF OBJECT_ID('tempdb..#utQ')IS NOT NULL DROP TABLE #utQ\n" +
            "SELECT ROW_NUMBER()OVER(ORDER BY w DESC)Q_ID, w\n" +
            "INTO #utQ\n" +
            "FROM(\n" +
            "  SELECT DISTINCT w\n" +
            "  FROM #LeaderAnswerList\n" +
            ")T\n" +
            "--SELECT *\n" +
            "--FROM #utQ\n" +
            "\n" +
            "INSERT INTO utV \n" +
            "SELECT N V_ID, LEFT(S,35)V_NAME, 'R' V_COLOR\n" +
            "FROM(VALUES\n" +
            "(139,'Painting schedule\t'),\n" +
            "(140,'Parents-descendants\t'),\n" +
            "(141,'Air-tours\t'),\n" +
            "(142,'Flying Control Organization\t'),\n" +
            "(143,'Cartel\t'),\n" +
            "(144,'Bowling\t'),\n" +
            "(145,'Sequence of natural numbers\t'),\n" +
            "(146,'Antivandal robot\t'),\n" +
            "(147,'Three subsets of paintings\t'),\n" +
            "(148,'Game `Life`\t'),\n" +
            "(149,'Multiplying\t'),\n" +
            "(150,'Two painting machines and maximal sets of squares\t'),\n" +
            "(151,'London-Vladivostok tours\t'),\n" +
            "(152,'Mars probe models\t'),\n" +
            "(153,'Emergency landing of planes\t'),\n" +
            "(154,'Convex polygon\t'),\n" +
            "(155,'A color spot\t'),\n" +
            "(156,'Common prime factors\t'),\n" +
            "(157,'All connected graphs\t'),\n" +
            "(158,'Dominoes\t'),\n" +
            "(159,'Matrix determinant\t'),\n" +
            "(160,'Substring of max length\t'),\n" +
            "(161,'Calculate an expression\t'),\n" +
            "(162,'The index of efficiency of each outlet point\t'),\n" +
            "(163,'Decimal fractions\t'),\n" +
            "(164,'Domination of the countries on the sea\t'),\n" +
            "(165,'One-armed bandit\t'),\n" +
            "(166,'List of q_id values in b_vol string\t'),\n" +
            "(167,'Maximal subsets of the ships of a country\t'),\n" +
            "(168,'War alliance\t'),\n" +
            "(169,'A number ciphered in a paint\t'),\n" +
            "(170,'Lengths having six V_IDs with absent color\t'),\n" +
            "(171,'Zugzwang\t'),\n" +
            "(172,'Game of tic-tac-toe\t'),\n" +
            "(173,'Segments with extremal sum of money\t'),\n" +
            "(174,'Light board at the airport\t'),\n" +
            "(175,'Sudoku\t'),\n" +
            "(176,'Periods of simultaneously being in flight of planes of not less than 3/4 of number of companies.\t'),\n" +
            "(177,'Determine character in cbaabaa\t'),\n" +
            "(178,'Transport Cost on Torus\t'),\n" +
            "(179,'Fibonacci Sequence\t'),\n" +
            "(180,'Raising b_v_id to b_q_id power\t'),\n" +
            "(181,'Possible coalitions of fought countries\t'),\n" +
            "(182,'MiniMax Autobus tours on Torus\t'),\n" +
            "(183,'Polygon that is represented by a convex hull\t'),\n" +
            "(184,'Digital talismen for Torus NxP planets\t'),\n" +
            "(185,'Compute C = A * B. For each of the digits 0 through 9 count how many times they occur in C\t'),\n" +
            "(186,'Distribution of incoming over payments and vice versa\t'),\n" +
            "(187,'Minimal subset of bound flights\t'),\n" +
            "(188,'Air-inspector\t'),\n" +
            "(189,'Torus population during dynasty Xor reign\t'),\n" +
            "(190,'Non-overlaping passenger groups\t'),\n" +
            "(191,'Circles sets\t'),\n" +
            "(192,'On the arithmetic''s lesson (2nd grade)\t'),\n" +
            "(193,'The equation of 5th degree\t'),\n" +
            "(194,'Code-tables\t'),\n" +
            "(195,'sur-competition\t'),\n" +
            "(196,'Maximal balance intervals\t'),\n" +
            "(197,'Find a celebrity\t'),\n" +
            "(198,'Unique triples of squares\t'),\n" +
            "(199,'Olympic airlines rating\t'),\n" +
            "(200,'Packing of computer goods\t'),\n" +
            "(201,'Division by baskets with NTILE\t'),\n" +
            "(202,'Booking places for a family\t'),\n" +
            "(203,'Game `Minesweeper` (text)\t'),\n" +
            "(204,'Equal numbers in different number systems\t'),\n" +
            "(205,'Sequence of S prime numbers greater than N\t'),\n" +
            "(206,'Tetrahedrons with the edges of different lengths\t'),\n" +
            "(207,'Quantity of lucky numbers in a roll\t'),\n" +
            "(208,'Coding ship name\t'),\n" +
            "(209,'Three characters in the ellipsoid\t'),\n" +
            "(210,'Minimal number of rearrangements when sorting\t'),\n" +
            "(211,'Happy painting\t'),\n" +
            "(212,'Commander’s Monitor\t'),\n" +
            "(213,'Four ships groups\t'),\n" +
            "(214,'Candidates and vacancies\t'),\n" +
            "(215,'Combinations of symbols in model number\t'),\n" +
            "(216,'Summing up vectors in subsets of a set\t'),\n" +
            "(217,'Fifty shades of grey\t'),\n" +
            "(218,'Basketball tournament\t'),\n" +
            "(219,'Three phones\t'),\n" +
            "(220,'220 volt\t'),\n" +
            "(221,'Marine dictionary\t'),\n" +
            "(222,'FIFO\t'),\n" +
            "(223,'Colored tuples R%G%B\t'),\n" +
            "(224,'The Sequel Finance Bank\t'),\n" +
            "(225,'Koch curve\t'),\n" +
            "(226,'Star wars\t'),\n" +
            "(227,'Thousand (game)\t'),\n" +
            "(228,'Chronofly\t'),\n" +
            "(229,'Run, rabbit, run.\t'),\n" +
            "(230,'singly linked lists\t'),\n" +
            "(231,'The labyrinth destroyed\t'),\n" +
            "(232,'Triangles with maximum perimeter\t'),\n" +
            "(233,'March, 8')\n" +
            ")T(N,S)\n" +
            "\n" +
            "INSERT INTO utQ\n" +
            "SELECT Q_ID, LEFT(IIF(w='Grinkevich D.L. (Dmitrij Grinkevich)','Grinkevich D.L.(Dmitrij Grinkevich)',w),35)\n" +
            "FROM #utQ\n" +
            "\n" +
            "INSERT utB\n" +
            "SELECT DATEADD(DD,dT,d), Q_ID, p, 1\n" +
            "FROM #LeaderAnswerList L\n" +
            "  JOIN #utQ Q ON Q.w = L.w\n" +
            "  CROSS JOIN (VALUES(0),(1),(2),(3),(4),(5),(6))T(dT)\n" +
            "\n" +
            "DECLARE @MSG NVARCHAR(1000) = (SELECT 'PRINT utB Count: ' + CAST((SELECT COUNT(*) FROM utB) AS NVARCHAR(1000)))\n" +
            "\n" +
            "PRINT @MSG\n" +
            "\n" +
            "if object_id('tempdb..#LeaderAnswerList')is not null drop table #LeaderAnswerList";

    private static final int FROM_NUMBER = 126;
    private static final int TO_NUMBER = 233;
    private static final String FILE_PATH = "/home/pegoopik/sql/leaders_2ndDB_script.sql";
    private static final String CREATE_TABLE_SQL = "delete from utB\ndelete from utQ\ndelete from utV\nGO\n" +
            "if object_id('tempdb..#LeaderAnswerList')is not null " +
            "drop table #LeaderAnswerList\n" +
            "create table #LeaderAnswerList(\n" +
            "p int,n int,w varchar(100),s int,t varchar(50),d varchar(50)\n)\n";

    @Ignore
    @SneakyThrows
    @Test
    public void test() {
        try(FileWriter writer = new FileWriter(FILE_PATH, false))
        {
            writer.append(CREATE_TABLE_SQL);
            for (int problemNumber = FROM_NUMBER; problemNumber <= TO_NUMBER ; problemNumber++) {
                writer.append("GO\n INSERT INTO #LeaderAnswerList SELECT * FROM (VALUES\n");
                List<LeaderAnswer> leaderAnswerList = getLeaderAnswerList(problemNumber);
                writer.append(StringUtils.join(leaderAnswerList, ",\n"));
                writer.append("\n)T(p,n,w,s,t,d)\n");
            }
            for (int problemNumber = 251; problemNumber <= 261 ; problemNumber++) {
                writer.append("GO\n INSERT INTO #LeaderAnswerList SELECT * FROM (VALUES\n");
                List<LeaderAnswer> leaderAnswerList = getLeaderAnswerList(problemNumber);
                writer.append(StringUtils.join(leaderAnswerList, ",\n"));
                writer.append("\n)T(p,n,w,s,t,d)\n");
            }
            for (int problemNumber = 301; problemNumber <= 307 ; problemNumber++) {
                writer.append("GO\n INSERT INTO #LeaderAnswerList SELECT * FROM (VALUES\n");
                List<LeaderAnswer> leaderAnswerList = getLeaderAnswerList(problemNumber);
                writer.append(StringUtils.join(leaderAnswerList, ",\n"));
                writer.append("\n)T(p,n,w,s,t,d)\n");
            }
            writer.append(SQL_END);
            writer.flush();
        }
    }

    private static List<LeaderAnswer> getLeaderAnswerList(Integer problemNumber) {
        List<LeaderAnswer> result = new ArrayList<>();
        Document document = Jsoup.parse(getSqlExHtmlList(problemNumber));
        Elements leaders = document.select("TABLE").select("[cellpadding=5]").get(2).child(0).children();
        for (Element leader : leaders) {
            LeaderAnswer leaderAnswer = new LeaderAnswer(problemNumber, leader);
            if (leaderAnswer.getNumber().contains("No")) {//строчка заголовков нам не нужна
                continue;
            }
            result.add(leaderAnswer);
        }
        return result;
    }

    @SneakyThrows
    private static String getSqlExHtmlList(Integer problemNumber) {
        Unirest.setProxy(new HttpHost("192.168.15.10", 8080));
        HttpResponse<String> response = Unirest.post("http://www.sql-ex.com/rating_3rd.php")
                .header("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                .header("Content-Type", "application/json")
                .header("Cache-Control", "no-cache")
                .header("Postman-Token", "dfc38270-08ba-e461-9d56-19b7a4d03fe6")
                .body("------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"nn4\"\r\n\r\n"+problemNumber+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--")
                .asString();
        return response.getBody();
    }

    @Getter
    private static class LeaderAnswer {
        private final Integer problemNumber;
        private final String number;
        private final String who;
        private final String seconds;
        private final String time;
        private final String date;
        LeaderAnswer(Integer problemNumber, Element leader) {
            this.problemNumber = problemNumber;
            number = leader.children().get(0).childNode(0).toString();
            who = leader.children().get(1).childNode(0).toString().replace("'","''");
            seconds = leader.children().get(2).childNode(0).toString();
            time = leader.children().get(3).childNode(0).toString();
            date = leader.children().get(4).childNode(0).toString();
        }
        @Override
        public String toString(){
            return "("+problemNumber+","+number+",'"+who+"',"+seconds+",'"+time+"','"+date+"')";
        }
    }
}
